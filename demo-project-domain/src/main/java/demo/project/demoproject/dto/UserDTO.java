package demo.project.demoproject.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
public class UserDTO {
    private String name;

    private String username;

    private String email;

    private String phone;

    public String toString() {
        return String.format("%1$s (email = %2$s)", name, email);
    }
}
