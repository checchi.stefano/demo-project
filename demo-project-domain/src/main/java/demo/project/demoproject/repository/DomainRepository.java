package demo.project.demoproject.repository;

import demo.project.demoproject.entity.Domain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DomainRepository extends JpaRepository<Domain, String> {
    @Modifying
    @Query("UPDATE Domain d SET d.occurrenceCount = d.occurrenceCount + 1 WHERE d.name = :domainName")
    void increaseOccurrenceCountByOne(@Param("domainName") String domainName);
}
