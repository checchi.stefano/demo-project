package demo.project.demoproject.entity;

import lombok.*;
import jakarta.persistence.*;

@Getter
@Setter
@ToString
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "domain")
public class Domain {
    @Id
    private String name;

    @Column(name = "occurrence_count")
    private Long occurrenceCount;
}
