package demo.project.demoproject.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;
import jakarta.persistence.*;
import javax.validation.constraints.Email;

@Getter
@Setter
@ToString(exclude = "post")
@EqualsAndHashCode(exclude = "post")
@Entity
@Table(name = "post_comment")
public class PostComment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_name")
    private String userName;

    @Email
    private String email;

    private String body;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) // , cascade = CascadeType.REFRESH
    @JoinColumn(name = "post_id", updatable = false)
    private Post post;
}
