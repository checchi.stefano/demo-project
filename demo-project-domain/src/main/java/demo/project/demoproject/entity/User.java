package demo.project.demoproject.entity;

import lombok.*;
import jakarta.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString(exclude = "posts")
@EqualsAndHashCode(exclude = "posts")
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(name = "username", updatable = false, unique = true)
    private String username;

    @Email
    @Column(name = "email", updatable = false, unique = true)
    private String email;

    @Column(name = "phone", updatable = false, unique = true)
    private String phone;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Post> posts = new HashSet<>();

    @Column(name = "created_at", updatable = false)
    private LocalDateTime createdAt;

    @PrePersist
    protected void onCreate() {
        if (createdAt == null) {
            createdAt = LocalDateTime.now();
        }
    }

    public String toString() {
        return String.format("%1$s (id = %2$s, email = %3$s)", name, id, email);
    }
}
