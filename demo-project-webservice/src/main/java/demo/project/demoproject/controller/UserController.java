package demo.project.demoproject.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import demo.project.demoproject.client.JSONPlaceholderClient;
import demo.project.demoproject.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name = "User API")
@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final JSONPlaceholderClient jsonPlaceholderClient;

    @Operation(summary = "Get all users")
    @GetMapping
    public ResponseEntity<List<UserDTO>> getAllUsers() {
        return ResponseEntity.ok(jsonPlaceholderClient.getUsers());
    }

    @Operation(summary = "Get user by id")
    @GetMapping("/{user_id}")
    public ResponseEntity<UserDTO> getUserById(@PathVariable("user_id") Long id) {
        return ResponseEntity.ok(jsonPlaceholderClient.getUserById(id));
    }
}
