package demo.project.demoproject.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import demo.project.demoproject.service.DomainService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Domain API")
@RestController
@RequestMapping("domains")
@RequiredArgsConstructor
public class DomainController {

    private final DomainService domainService;

    @Operation(summary = "Get domain count by domain name")
    @GetMapping("/{domain_name}")
    public ResponseEntity<Long> getDomainCountByDomainName(@PathVariable("domain_name") String domainName) {
        return ResponseEntity.ok(domainService.getDomainCountByDomainName(domainName));
    }
}
