# Launching the application
### For Mac Users

* Install `homebrew` (https://brew.sh/)
* Install Kafka by entering the following command in your terminal: `brew install kafka`
* Run in the terminal the two following commands:
````
zookeeper-server-start -daemon /opt/homebrew/etc/kafka/zookeeper.properties |
kafka-server-start -daemon /opt/homebrew/etc/kafka/server.properties
````
* Run `mvn clean install` on the application
* Follow the instructions in the `docker-dev-tools` directory to create a `MariaDB` instance locally
* Create a database (for convenience, we suggest you name it `demoproject`)
* Run liquibase locally by executing the following command from the `demo-project-webservice` module:
```
mvn liquibase:update -Dliquibase.url=jdbc:mariadb://localhost:3306/demoproject -Dliquibase.changeLogFile=db/changelog/db.changelog-master.xml -Dliquibase.password=rootpwd -Dliquibase.username=root
```
* Run the `DemoProjectApplication`, configuring environment variables as needed

For instance:
````
SPRING_DATASOURCE_URL=jdbc:mariadb://localhost:3306/demoproject
SPRING_DATASOURCE_PASSWORD=rootpwd
SPRING_DATASOURCE_USERNAME=root
````
Once the application is launched, all REST requests can be handled via the Swagger UI at: `http://localhost:8080/swagger-ui/index.html`