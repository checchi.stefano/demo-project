package demo.project.demoproject.helper;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static demo.project.demoproject.helper.EmailUtils.getDomainName;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EmailUtilsTest {
    @Nested
    @DisplayName("Get domain name")
    class GetDomainNameTest {
        @ParameterizedTest
        @DisplayName("Should fail when receiving an empty string")
        @ValueSource(strings = {"", "    "})
        void test_failure_emptyString(String emptyString) {
            assertThrows(IllegalArgumentException.class, () -> getDomainName(emptyString));
        }

        @ParameterizedTest
        @DisplayName("Should fail when receiving an invalid email address")
        @ValueSource(strings = {"fake", "kafe@fake@com"})
        void test_failure_invalidEmail(String emptyString) {
            assertThrows(IllegalArgumentException.class, () -> getDomainName(emptyString));
        }

        @ParameterizedTest
        @DisplayName("Should return the domain name when receiving a valid email address")
        @CsvSource({"real@gmail.com,gmail.com", "   abaababa@yahoo.it        ,yahoo.it"})
        void test_success_validEmail(String email, String expectedDomain) {
            String actualDomain = getDomainName(email);
            assertThat(actualDomain).isEqualTo(expectedDomain);
        }
    }
}
