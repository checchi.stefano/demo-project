package demo.project.demoproject.client;

import demo.project.demoproject.dto.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "jsonPlaceholderClient", url = "${environment.jsonplaceholder.endpoint}")
public interface JSONPlaceholderClient {
    @GetMapping("/users")
    List<UserDTO> getUsers();

    @GetMapping("/users/{user_id}")
    UserDTO getUserById(@PathVariable("user_id") Long id);
}
