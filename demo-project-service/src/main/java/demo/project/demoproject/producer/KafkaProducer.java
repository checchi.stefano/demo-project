package demo.project.demoproject.producer;

import demo.project.demoproject.client.JSONPlaceholderClient;
import demo.project.demoproject.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Random;
import java.time.LocalDateTime;

import static demo.project.demoproject.config.KafkaTopicConfiguration.USER_TOPIC;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaProducer {
    private final KafkaTemplate<String, UserDTO> kafkaTemplate;
    private final JSONPlaceholderClient userClient;
    private final Random random;

    @Scheduled(fixedRate = 10)
    public void sendMessage() {

        long userId = random.nextInt(10) + 1;// ten users
        log.info("using id: {}", userId);
        UserDTO user = userClient.getUserById(userId); // retrieves information on the user from the Fake REST API

        log.info("Kafka Message: user {} access at: {}", user, LocalDateTime.now());
        kafkaTemplate.send(USER_TOPIC, user); // publishes to the topic
    }
}
