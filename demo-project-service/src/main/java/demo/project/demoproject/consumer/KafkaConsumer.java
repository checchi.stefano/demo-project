package demo.project.demoproject.consumer;

import demo.project.demoproject.dto.UserDTO;
import demo.project.demoproject.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import static demo.project.demoproject.config.KafkaTopicConfiguration.*;


@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaConsumer {
    private final UserService userService;

    @KafkaListener(topics = USER_TOPIC, groupId = USER_GROUP)
    public void consume(UserDTO user) {
        log.info("Kafka :: Received user {}", user);
        userService.process(user);
    }
}