package demo.project.demoproject.helper;

import static org.apache.logging.log4j.util.Strings.isBlank;

public class EmailUtils {

    private EmailUtils() {
        throw new UnsupportedOperationException("Utility classes should not be instantiated.");
    }

    public static String getDomainName(String email) {
        if (isBlank(email)) {
            throw new IllegalArgumentException("Email address should not be null or empty.");
        }

        String[] parts = email.trim().split("@");
        if (parts.length != 2) {
            throw new IllegalArgumentException("Invalid email address format.");
        }

        return parts[1];
    }
}
