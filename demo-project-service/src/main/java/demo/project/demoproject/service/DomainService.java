package demo.project.demoproject.service;

import demo.project.demoproject.repository.DomainRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DomainService {

    private final DomainRepository domainRepository;

    public Long getDomainCountByDomainName(String domainName) {
        var domain = domainRepository.findById(domainName).orElseThrow(
                () -> new RuntimeException("No domain found for name: " + domainName));

        return domain.getOccurrenceCount();
    }
}
