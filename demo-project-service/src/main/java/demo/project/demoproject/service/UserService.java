package demo.project.demoproject.service;

import demo.project.demoproject.dto.UserDTO;
import demo.project.demoproject.entity.Domain;
import demo.project.demoproject.helper.EmailUtils;
import demo.project.demoproject.mapper.UserMapper;
import demo.project.demoproject.repository.DomainRepository;
import demo.project.demoproject.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final DomainRepository domainRepository;

    private final UserMapper userMapper;

    /**
     * Saves information on users if it is the first time they access the platform.<br/>
     * Extracts the domain name from the user's email and increases the number of its occurrences by one.
     *
     * @param userDTO the DTO representing a user.
     */
    @Transactional
    public void process(UserDTO userDTO) {
        if (!userRepository.existsByEmail(userDTO.getEmail()))
            userRepository.save(userMapper.toEntity(userDTO));

        String domain = EmailUtils.getDomainName(userDTO.getEmail());
        if (domainRepository.existsById(domain)) {
            domainRepository.increaseOccurrenceCountByOne(domain);
        } else {
            domainRepository.save(new Domain(domain, 1L));
        }

        // call to REST APIs ... ?
    }


}
