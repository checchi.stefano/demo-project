package demo.project.demoproject.mapper;

import demo.project.demoproject.dto.UserDTO;
import demo.project.demoproject.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User toEntity(UserDTO dto);

    UserDTO toDto(User entity);
}
